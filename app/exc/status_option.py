from http import HTTPStatus

class StatusOptionsError(Exception):
    
#    feild_options = ["alive", "dead", "unknown"]

    def __init__(self, status: str,field_options:list) -> None:
        self.message = (
            {
                "error": {
                    "status_options": field_options,
                    "recieved_option": status,
                }
            },
            HTTPStatus.BAD_REQUEST
        )

        super().__init__(self.message)
