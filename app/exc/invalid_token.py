from http import HTTPStatus

class InvalidTokenError(Exception):
    def __init__(self):
        self.message=(
            {
            "error":"invalid token"
            },
         HTTPStatus.UNAUTHORIZED,
         )

        super().__init__(self.message)