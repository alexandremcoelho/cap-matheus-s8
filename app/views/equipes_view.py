from flask import Blueprint, current_app, json, request, jsonify
from http import HTTPStatus
from app.services import criar_equipe, equipe_tarefas_helpers,obter_equipes,deletar_equipes
from app.models import Empresa, Equipes, EquipeTarefas, Tarefas 
from app.exc import MissingKeyError, RequiredKeyError
from sqlalchemy.exc import IntegrityError
from flask_jwt_extended import jwt_required,get_jwt_identity
from app.services.equipe_tarefas_helpers import filter_task_by_id


bp = Blueprint("equipe_blueprint", __name__,url_prefix="/equipes")

@bp.post("/")
@jwt_required()
def create_equipe():
    data = request.get_json()
    try:
        return criar_equipe(data), HTTPStatus.CREATED

    except MissingKeyError as e:
        return e.message

    except RequiredKeyError as e:
        return e.message

    except IntegrityError as e:
        return {"ERROR": "dados invalidos"}, HTTPStatus.BAD_REQUEST

@bp.delete("/deletar_equipe/<int:equipe_id>")
@jwt_required()
def delete_equipe(equipe_id):
    empresa_token = get_jwt_identity()
    empresa = Empresa.query.filter_by(email=empresa_token).first()
    try:
        return deletar_equipes(equipe_id,empresa),HTTPStatus.OK
    except IntegrityError as e:
        return {"ERROR":"insira dados de uma equipe da sua empresa"},HTTPStatus.BAD_REQUEST   

@bp.get("/")
@jwt_required()
def equipe():    
    sended_company=get_jwt_identity()
    company=Empresa.query.filter_by(email=sended_company).first() 
    return obter_equipes(company),HTTPStatus.OK


@bp.get("/tarefas")
@jwt_required()
def equipe_tarefa():    
    tarefa_id = request.args.get("tarefa_id")

    return jsonify(filter_task_by_id(tarefa_id)), HTTPStatus.OK