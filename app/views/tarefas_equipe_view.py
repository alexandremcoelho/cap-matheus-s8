from http import HTTPStatus

from app.exc import MissingKeyError
from app.services import criar_tarefa_equipe
from flask import Blueprint, request

bp = Blueprint('bp_tarefas_equipes_routes',__name__,url_prefix='/tarefa')


@bp.post('/equipe_tarefa')
def create():
    data = request.get_json()
    return criar_tarefa_equipe(data), HTTPStatus.CREATED

