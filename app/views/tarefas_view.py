from http import HTTPStatus

from app.exc import MissingKeyError
from app.services import atualizar_tarefa, criar_tarefa, obter_tarefas, deletar_tarefa
from flask import Blueprint, request
from flask_jwt_extended import jwt_required

bp = Blueprint('bp_tarefas_routes',__name__,url_prefix='/tarefa')

@bp.route('/tarefas', methods=['POST'])
@jwt_required()
def create():
    data = request.get_json() 
    return criar_tarefa(data)

@bp.post('/')
@jwt_required()
def criar_tarefas():
    data=request.get_json()
    try:
        return criar_tarefa(data),HTTPStatus.CREATED
    except MissingKeyError as e:
        return e.message 

@bp.get('/')
@jwt_required()
def get_tarefas():
    status = request.args.get("status")
    prioridade = request.args.get("prioridade")
    print(prioridade,status)
    return obter_tarefas(prioridade,status),HTTPStatus.OK

@bp.patch('/atualizar_tarefa')
@jwt_required()
def update_tarefas():
    data = request.get_json()
    try:
        return atualizar_tarefa(data),HTTPStatus.CREATED
    except MissingKeyError as e:
        return e.message 

@bp.delete("/delete/<int:tarefa_id>")
@jwt_required()
def delete_tarefa(tarefa_id):
    try: 
        return deletar_tarefa(tarefa_id),HTTPStatus.OK
    except:
        return {"ERROR":"insira dados de um colaborador da sua empresa"}, HTTPStatus.BAD_REQUEST





    
