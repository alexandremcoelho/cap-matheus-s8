from app import exc
from flask import Blueprint, request
from app.services import criar_prioridade,obter_prioridades
from http import HTTPStatus
from app.exc import (    
    MissingKeyError,    
    StatusOptionsError,
)

bp = Blueprint('bp_prioridades_routes', __name__,url_prefix='/prioridade')

@bp.post('/')
def create_prioridade():
    data = request.get_json()
    try:
        return  criar_prioridade(data), HTTPStatus.CREATED
    except MissingKeyError as e:
        return e.message
    except StatusOptionsError as e:
         return e.message
@bp.get('/')    
def get_prioridade():
    return obter_prioridades(),HTTPStatus.OK






