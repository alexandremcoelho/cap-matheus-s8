from flask import Flask,Blueprint,jsonify,request 
from flask_jwt_extended import get_jwt_identity, jwt_required
from app.services import criar_hardSkill,relacionar_hardSkill 
from app.exc import MissingKeyError,StatusOptionsError,InvalidTokenError
from http import HTTPStatus

bp=Blueprint("hard_skill_blueprint",__name__,url_prefix="/hard_skill")

@bp.get("/")
@jwt_required()
def get_hardSkill():
    from app.models import HardSkill
    hardSkills = HardSkill.query.all()
    hardSkill=[item.serialized for item in hardSkills]
    return jsonify(hardSkill),HTTPStatus.OK 

@bp.post("/")
@jwt_required()
def create_hardSkill():
    from app.models import Empresa
    current_user = get_jwt_identity()
    empresa = Empresa.query.filter_by(email=current_user).first()
    data=request.get_json()
    
    try:
        return criar_hardSkill(data,empresa),HTTPStatus.CREATED
    except MissingKeyError as e:
        return e.message, HTTPStatus.BAD_REQUEST
    except StatusOptionsError as e:
        return e.message, HTTPStatus.BAD_REQUEST
    except InvalidTokenError as e:      
        return e.message, HTTPStatus.BAD_REQUEST   

@bp.post("/colaborador")  
@jwt_required()
def colaborador_hard_skill():
    from app.models import Empresa
    current_user = get_jwt_identity()
    empresa = Empresa.query.filter_by(email=current_user).first()
    data= request.get_json()

    try:
        return relacionar_hardSkill(data,empresa),HTTPStatus.CREATED 
    except MissingKeyError as e:
        return e.message
    except InvalidTokenError as e:      
        return e.message          
