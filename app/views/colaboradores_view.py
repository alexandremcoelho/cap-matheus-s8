from app.models import Empresa
from flask import Blueprint,request,jsonify
from http import HTTPStatus
from app.exc import MissingKeyError, RequiredKeyError,InvalidTokenError
from sqlalchemy.exc import IntegrityError
from flask_jwt_extended import jwt_required,get_jwt_identity
from app.services import criar_colaborador,obter_colaboradores,login_colaborador, entrar_equipe, obter_colaboradores_skill,obter_colaboradores_equipe,obter_colaboradores_alocados, deletar_colaborador

bp = Blueprint('bp_calaboradores_routes', __name__,url_prefix="/colaboradores")

@bp.get('/')
@jwt_required()
def get_colaboradores():
    return obter_colaboradores(),HTTPStatus.OK

@bp.post("/create_acount/<int:empresa_id>")
def signup_colaboradores(empresa_id:int):
    data = request.get_json()
    data["empresa_id"] = empresa_id
    data["alocado"]=False
    try:
        return criar_colaborador(data), HTTPStatus.CREATED
    except MissingKeyError as e:
        return e.message, HTTPStatus.BAD_REQUEST

@bp.delete("/delete_acount/<int:colaborador_id>")
@jwt_required()
def delete_colaborador(colaborador_id):
    empresa_token = get_jwt_identity()
    empresa = Empresa.query.filter_by(email=empresa_token).first()
    try: 
        return deletar_colaborador(colaborador_id,empresa),HTTPStatus.OK
    except IntegrityError as e:
        return {"ERROR":"insira dados de um colaborador da sua empresa"}   

@bp.post("/login") 
def login_colaborador_view():
    data = request.get_json() 
    try:
        return login_colaborador(data),HTTPStatus.OK
    except MissingKeyError as e:
        return e.message, HTTPStatus.BAD_REQUEST

@bp.patch("/equipe") 
@jwt_required() 
def join_colaborador_view(): 
    data = request.get_json()
    empresa_token = get_jwt_identity()
    empresa = Empresa.query.filter_by(email=empresa_token).first()
    try:
        return entrar_equipe(data,empresa), HTTPStatus.CREATED

    except MissingKeyError as e:
        return e.message, HTTPStatus.BAD_REQUEST
    except RequiredKeyError as e:
        return e.message, HTTPStatus.BAD_REQUEST
    except InvalidTokenError as e:
        return e.message, HTTPStatus.BAD_REQUEST
    except IntegrityError as e:
        return {"ERROR": "dados invalidos"}, HTTPStatus.BAD_REQUEST

@bp.get("/equipe/<int:equipe_id>")
@jwt_required()
def get_equipe_colaborador(equipe_id):
    empresa_token = get_jwt_identity()
    empresa = Empresa.query.filter_by(email=empresa_token).first() 
    return obter_colaboradores_equipe(equipe_id,empresa), HTTPStatus.OK

@bp.get("/alocados")
@jwt_required()
def get_colaboradores_alocados():
    empresa_token = get_jwt_identity()
    empresa = Empresa.query.filter_by(email=empresa_token).first() 
    return obter_colaboradores_alocados(empresa),HTTPStatus.OK 

@bp.get("/skill")
@jwt_required()
def get_colaborador_soft():
    soft =request.args.get("softskill_id")
    hard =request.args.get("hardskill_id") 
    empresa_token = get_jwt_identity()
    empresa = Empresa.query.filter_by(email=empresa_token).first() 

    return obter_colaboradores_skill(hard,soft,empresa), HTTPStatus.OK
