from flask import Blueprint, request
from app.exc import (    
    MissingKeyError,    
    StatusOptionsError,
)
from http import HTTPStatus
from app.services import criar_status,obter_status

bp = Blueprint('bp_status_route', __name__,url_prefix="/status")
@bp.get("/")
def get_status():
    return obter_status(),HTTPStatus.OK

@bp.post('/')
def create_status():
    data = request.get_json()    
    try:
        return criar_status(data), HTTPStatus.CREATED
    except MissingKeyError as e:
        return e.message
    except StatusOptionsError as e:
         return e.message      
