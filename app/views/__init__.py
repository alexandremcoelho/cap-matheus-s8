from flask import Flask


def init_app(app: Flask):
    from .colaboradores_view import bp as bp_colaboradores
    from .empresa_view import bp as bp_empresa
    from .equipes_view import bp as bp_equipes
    from .hardSkill_view import bp as bp_hardSkilll
    from .prioridade_view import bp as bp_prioridades
    from .softSkill_view import bp as bp_softSkill
    from .status_view import bp as bp_status
    from .tarefas_equipe_view import bp as bp_tarefas_equipes
    from .tarefas_view import bp as bp_tarefas

    app.register_blueprint(bp_colaboradores)

    app.register_blueprint(bp_prioridades)    
    app.register_blueprint(bp_status)
    app.register_blueprint(bp_tarefas)
    
    app.register_blueprint(bp_empresa)
    app.register_blueprint(bp_equipes)
    app.register_blueprint(bp_hardSkilll)
    app.register_blueprint(bp_softSkill)
    app.register_blueprint(bp_tarefas_equipes)
