from flask import Blueprint,jsonify,request 
from flask_jwt_extended import get_jwt_identity, jwt_required
from app.exc import MissingKeyError,InvalidTokenError
from app.services import criar_softSkill,relacionar_softSkill
from http import HTTPStatus

bp = Blueprint("bp_softSkill",__name__,url_prefix="/soft_skill")

@bp.get("/")
@jwt_required()
def get_softSkill():
    from app.models import SoftSkill
    softSkills = SoftSkill.query.all()
    softSkill = [item.serialized for item in softSkills]
    return jsonify(softSkill)
@bp.post("/")
@jwt_required()
def create_softSkill():
    from app.models import Empresa
    data = request.get_json()
    empresa_token=get_jwt_identity()
    empresa=Empresa.query.filter_by(email=empresa_token).first()
    try:
        return criar_softSkill(data,empresa), HTTPStatus.CREATED
    except MissingKeyError as e:
        return e.message

@bp.post("/colaborador")
@jwt_required()
def colaborador_soft_skill():
    from app.models import Empresa
    current_user = get_jwt_identity()
    empresa = Empresa.query.filter_by(email=current_user).first()
    data= request.get_json()

    try:
        return relacionar_softSkill(data,empresa),HTTPStatus.CREATED 
    except MissingKeyError as e:
        return e.message
    except InvalidTokenError as e:      
        return e.message           

