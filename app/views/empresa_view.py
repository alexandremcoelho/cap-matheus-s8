from flask import Blueprint, current_app, request, jsonify
from http import HTTPStatus
from sqlalchemy import exc
from app.services.empresa_helpers import criar_empresa, login_empresa
from app.services.helpers import delete_commit
from app.exc import MissingKeyError, RequiredKeyError
from app.models import Empresa
from sqlalchemy.exc import IntegrityError
from flask_jwt_extended import jwt_required,get_jwt_identity


bp = Blueprint("empresa_blueprint", __name__)


@bp.post("/empresa")
def create_empresa():
    data = request.get_json()
    try:
        return criar_empresa(data), HTTPStatus.CREATED

    except MissingKeyError as e:
        return e.message

    except RequiredKeyError as e:
        return e.message
    except IntegrityError as e:
        return {"ERROR": "dados invalidos"}, HTTPStatus.BAD_REQUEST


@bp.get("/empresas")
def empresas():
    lista = Empresa.query.all()
    lista = [item.serialized for item in lista]
    return jsonify(lista), HTTPStatus.OK


@bp.post("/empresa/login")
def login_colaborador_view():
    data = request.get_json()
    key_list = ["email", "password"]

    try:
        if "incorrect email or password" in login_empresa(data).values():
            return login_empresa(data), HTTPStatus.UNAUTHORIZED
        return jsonify(login_empresa(data))

    except:
        return {"campos_requeridos": key_list,
                "campos_recebidos": [key for key in data.keys()]
                }, HTTPStatus.UNPROCESSABLE_ENTITY


@bp.delete("/empresa/delete/<int:empresa_id>")
@jwt_required()
def delete_empresa(empresa_id):
    empresa_token = get_jwt_identity()
    empresa = Empresa.query.filter_by(email=empresa_token).first()
    if empresa.id == empresa_id:
        try: 
            delete_commit(empresa)
            return {},HTTPStatus.OK
        except e:
            return {"ERROR":"token invalido"}, HTTPStatus.BAD_REQUEST
    else:
        return {"ERROR": "id invalido"},HTTPStatus.BAD_REQUEST