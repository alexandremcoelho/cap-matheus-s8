from app.models import Colaboradores,Empresa,HardSkillColaborador as hardcolab,SoftSkillColaborador as Softcolab 
from flask import jsonify

def obter_colaboradores():
    colaboradores = Colaboradores.query.all()   
    colaborador = [item.serialized for item in colaboradores]    
    for colab in colaborador:
        empresa = Empresa.query.filter_by(id = colab["empresa"]).first()
        colab["empresa"]=empresa.razao_social        
    return jsonify(colaborador) 

def obter_colaboradores_equipe(equipe:int,empresa:object):
    colaboradores = Colaboradores.query.filter_by(equipe_id=equipe).all()
    colaboradores_empresa = [colaborador.serialized for colaborador in colaboradores if colaborador.empresa_id == empresa.id]

    for col in colaboradores_empresa: 
        empresa = Empresa.query.filter_by(id = col["empresa"]).first()
        col["empresa"]=empresa.razao_social

    return jsonify(colaboradores_empresa)

def obter_colaboradores_alocados(empresa:object):
    colaboradores = Colaboradores.query.all()
    colaboradores_empresa = [colaborador.serialized for colaborador in colaboradores if colaborador.empresa_id == empresa.id]
    ocupados=[]
    desocupados = []
    for col in colaboradores_empresa: 
        empresa = Empresa.query.filter_by(id = col["empresa"]).first()
        col["empresa"]=empresa.razao_social
        if col["alocado"] == True:
            ocupados.append(col)
        else:
            desocupados.append(col)
    return {"alocados":ocupados, "desalocados":desocupados}    

def colaboradores_hardSkill(hard:int,empresa:object):
    hard_list=hardcolab.query.filter_by(hard_skill_id=hard).all()                   
    colab_list1 = [Colaboradores.query.filter_by(id=obj.colaborador_id).first() for obj in hard_list ]
    out_list = [obj.serialized for obj in colab_list1 if obj.empresa_id == empresa.id]   

    return out_list 

def colaboradores_softSkill(soft:int,empresa:object):   
    soft_list=Softcolab.query.filter_by(soft_skill_id=soft).all()                   
    colab_list2 = [Colaboradores.query.filter_by(id=obj.colaborador_id).first() for obj in soft_list ]
    out_list = [obj.serialized for obj in colab_list2 if obj.empresa_id == empresa.id]    

    return out_list  

def obter_colaboradores_skill(hard:int,soft:int,empresa:object):
    if hard and soft:
         hardSkill = colaboradores_hardSkill(hard,empresa)
         softSkill = colaboradores_softSkill(soft,empresa)                
         return {"hard_skill_list": hardSkill,"soft_skill_list":softSkill}                          
    if hard:
      return jsonify(colaboradores_hardSkill(hard,empresa))     
    
    return jsonify(colaboradores_softSkill(soft,empresa))            