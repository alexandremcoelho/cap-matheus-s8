from app.exc import (    
    MissingKeyError,        
)
from app.models import Tarefas
from .helpers import verify_missing_key,verify_recieved_key, add_commit, delete_commit
from sqlalchemy.exc import IntegrityError

def criar_tarefa(data:dict):
    key_list=["nome","data_inicio","data_fim","descrição"]
    if verify_missing_key(data,key_list):
        raise MissingKeyError(data, key_list)
    try:
        new_tarefa=Tarefas(**data)
        add_commit(new_tarefa)
    except IntegrityError:
        return {"ERROR": "dados invalidos"} 
    return {
            "nome":new_tarefa.nome,
            "data_inicio":new_tarefa.data_inicio,
            "data_fim:":new_tarefa.data_fim,
            "descrição":new_tarefa.descrição
        }   

def atualizar_tarefa(data):
    key_list=["id","nome","data_inicio","data_fim","descrição","status_id","prioridade_id","equipe_id"]
    required_key=["id"]
    if verify_missing_key(data,required_key):
        return {"ERROR":"deve ser enviado o id da tarefa"}
    if verify_recieved_key(data,key_list):
        raise MissingKeyError(data,key_list)
    try:
        tarefa=Tarefas.query.get(data["id"])
        for key,value in data.items():
            setattr(tarefa,key,value)
        add_commit(tarefa)
    except IntegrityError:
        return {"ERROR": "dados invalidos"} 
    return{
       "nome":tarefa.nome,
       "data_inicio":tarefa.data_inicio,
       "data_fim:":tarefa.data_fim,
       "descrição":tarefa.descrição,
       "status":tarefa.status_id,
       "prioridade":tarefa.prioridade_id,
       "equipe":tarefa.equipe_id
    }         

def deletar_tarefa(tarefa_id:int):
    tarefa = Tarefas.query.filter_by(id=tarefa_id).first()    
    try:
        delete_commit(tarefa)
        return {}
    except:
        return {"ERROR":"id incorreto"}   