from flask import current_app

from flask_sqlalchemy.model import Model


def verify_missing_key(data: dict, required_keys: list) -> list:
    data_keys = data.keys()

    return [key for key in required_keys if key not in data_keys]


def verify_recieved_key(data: dict, key_list: list) -> list:
    data_keys = data.keys()

    return [key for key in data_keys if key not in key_list]

def invalid_status_option(data_status:str,status_list:list):
    if data_status not in status_list:
        return True
    return False    

def add_all_commit(list_model: list[Model]) -> None:
    current_app.db.session.add_all(list_model)
    current_app.db.session.commit()

def add_commit(model: Model) -> None:
    current_app.db.session.add(model)
    current_app.db.session.commit()

def delete_commit(model:Model) -> None:
    current_app.db.session.delete(model)
    current_app.db.session.commit()
