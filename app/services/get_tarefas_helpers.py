from app.models import Tarefas, Status, Prioridade
from flask import jsonify

def obter_tarefa_status(status_nome:str):
    status_selecionado = Status.query.filter(Status.status.ilike(f'%{status_nome}%')).first()
    if status_selecionado:
        tarefas = Tarefas.query.filter_by(status_id=status_selecionado.id).all()
        tarefa = [item.serialized for item in tarefas]
        return tarefa
    return status_selecionado    

def obter_tarefa_prioridade(prioridade_nome:str):
    prioridade_selecionada = Prioridade.query.filter(Prioridade.prioridade.ilike(f'%{prioridade_nome}%')).first()
    if prioridade_selecionada:
        tarefas = Tarefas.query.filter_by(prioridade_id=prioridade_selecionada.id).all()
        tarefa = [item.serialized for item in tarefas]
        return tarefa
    return prioridade_selecionada    

def obter_tarefas(prioridade_nome:str,status_nome:str):
    prioridades=obter_tarefa_prioridade(prioridade_nome)
    status=obter_tarefa_status(status_nome)
    if prioridades and not status:
        print("ola")
        return jsonify(prioridades)
    if status and not prioridades:
        return jsonify(status) 
    if status and prioridades:       
        return {"prioridade":prioridades, "status":status} 
    print(prioridades,status)       
    tarefas = Tarefas.query.all()
    tarefa=[item.serialized for item in tarefas]
    return jsonify(tarefa)
    