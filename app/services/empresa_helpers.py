from app.exc import (
    GenderOptionsError,
    StatusOptionsError,
    MissingKeyError,
    RequiredKeyError,
)
from app.models import Empresa
from .helpers import verify_recieved_key, verify_missing_key, add_all_commit, add_commit
from flask_jwt_extended import create_access_token
from sqlalchemy.exc import IntegrityError
from flask import jsonify
from http import HTTPStatus


def criar_empresa(data: dict):
    key_list = ["nome", "razao_social", "nome_fantasia",
                "cnpj", "telefone", "email", "password"]

    if verify_missing_key(data, key_list):
        raise MissingKeyError(data, key_list)

    try:
        password_to_hash = data.pop("password")
        new_char = Empresa(**data)
        new_char.password = password_to_hash
        add_commit(new_char)
    except IntegrityError as e:
        return {"ERROR": "dados invalidos"}

    return new_char.serialized

def login_empresa(data):
    key_list = ["email", "password"]

    if verify_missing_key(data, key_list):
        return {data}

    found_empresa = Empresa.query.filter_by(email=data["email"]).first()

    if not found_empresa:
        return {"message": "incorrect email or password"}

    if found_empresa.verify_password(data["password"]):
        access_token = create_access_token(
            identity=found_empresa.email)
        return {"access_token": access_token}

    return {"message": "incorrect email or password"}
