from app.exc import (    
    MissingKeyError,    
    StatusOptionsError,
    InvalidTokenError
)
from app.models import HardSkill,HardSkillColaborador,Colaboradores
from .helpers import verify_missing_key, add_all_commit, add_commit,invalid_status_option
from sqlalchemy.exc import IntegrityError

def criar_hardSkill(data:dict,user:object):
    key_list = ["skill","experiencia"]
    experience_list = ["alta","media","baixa"]
    
    if verify_missing_key(data, key_list):
        raise MissingKeyError(data, key_list)
    if invalid_status_option(data["experiencia"],experience_list):
        raise StatusOptionsError(data["experiencia"],experience_list)  
    if user == None:      
        raise InvalidTokenError()
    try:
        new_skill=HardSkill(**data)
        add_commit(new_skill)    
    except IntegrityError as e:
        return {"ERROR": "dados invalidos"} 

    return {
        "skill":data["skill"],
        "experiencia":data["experiencia"],
        }    

def relacionar_hardSkill(data:dict,empresa:object):
    key_list=["colaborador_id","hard_skill_id"]
    if verify_missing_key(data, key_list):
        raise MissingKeyError(data, key_list)
    if empresa == None:
        raise InvalidTokenError()
    try:
        new_relation=HardSkillColaborador(**data)
        add_commit(new_relation)
    except IntegrityError as e:
        return {"ERROR": "dados invalidos"} 
    colaborador = Colaboradores.query.filter_by(id=new_relation.colaborador_id).first() 
    skill = HardSkill.query.filter_by(id = new_relation.hard_skill_id).first()
    return {"colaborador": colaborador.nome,"hard_skill":skill.skill}               
