from app.exc import (    
    MissingKeyError, 
    InvalidTokenError       
)
from app.models import Colaboradores,Empresa,Equipes
from .helpers import verify_missing_key, add_commit, delete_commit
from flask_jwt_extended import create_access_token
from sqlalchemy.exc import IntegrityError
from flask import jsonify

def criar_colaborador(data):
    key_list=["nome","email","password"] 
    empresa = Empresa.query.filter_by(id = data["empresa_id"]).first()
    
    if verify_missing_key(data,key_list):
        raise MissingKeyError(data, key_list)
    try:
        password_to_hash = data.pop("password")
        new_col = Colaboradores(**data)
        new_col.password = password_to_hash        
        add_commit(new_col)     
    except IntegrityError as e:
        return {"ERROR": "dados invalidos"}
    return {
        "nome":new_col.nome,
        "email":new_col.email,
        "alocado":new_col.alocado,        
        "empresa":empresa.razao_social
    }

def deletar_colaborador(colaborador_id:int,empresa:object):
    colaboradores_empresa = Colaboradores.query.filter_by(empresa_id=empresa.id).all()
    colaborador = [colab for colab in colaboradores_empresa if colab.id == colaborador_id]
    colaborador_deletado = Colaboradores.query.get(colaborador_id)       
    if colaborador:
        delete_commit(colaborador_deletado)
        return {}  
    else:    
        return {"ERROR":"insira dados de um colaborador da sua empresa"}     

def login_colaborador(data):
    key_list = ["email","password"]  
    if verify_missing_key(data,key_list):
        raise MissingKeyError(data, key_list)  
    found_colaborador=Colaboradores.query.filter_by(email=data["email"]).first() 
    if not found_colaborador:
        return {"message": "User not found"}
    try:
       if found_colaborador.verify_password(data["password"]):
            access_token = create_access_token(identity=found_colaborador.email)            
    except IntegrityError as e:
        return {"message": "Unauthorized"}
    return jsonify(access_token=access_token)    

def entrar_equipe(data: dict,empresa:object):
    key_list = ["colaborador_id", "equipe_id"]
    data["alocado"]=True
    if verify_missing_key(data, key_list):
        raise MissingKeyError(data, key_list)
    if empresa == None:
        raise InvalidTokenError()    
    try:
        colaborador = Colaboradores.query.get(data["colaborador_id"])
        for key,value in data.items():
            setattr(colaborador,key,value)
        add_commit(colaborador)          
    except IntegrityError as e:
        return {"ERROR": "dados invalidos"}    

    equipe = Equipes.query.filter_by(id = data["equipe_id"]).first()
    return {
        "colaborador_id": colaborador.nome,
        "equipe_id":equipe.nome,
    }


