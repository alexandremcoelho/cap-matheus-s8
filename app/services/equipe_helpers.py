from app.exc import (
    GenderOptionsError,
    StatusOptionsError,
    MissingKeyError,
    RequiredKeyError,
)
from app.models import Equipes
from .helpers import verify_recieved_key, verify_missing_key, delete_commit, add_commit
from flask import jsonify
from sqlalchemy.exc import IntegrityError


def criar_equipe(data: dict):
    key_list = ["nome", "gerente_de_equipes", "descrição", "empresa_id", "especialização"]

    if verify_missing_key(data, key_list):
        raise MissingKeyError(data, key_list)

    new_char = Equipes(**data)
    add_commit(new_char)

    return {
        "name": new_char.nome,
        "descricao": new_char.descrição,
    }

def deletar_equipes(equipe_id:int,empresa:object):
    equipes_empresa = Equipes.query.filter_by(empresa_id=empresa.id).all()
    equipe = [equi for equi in equipes_empresa if equi.id == equipe_id]
    equipe_deletada = Equipes.query.get(equipe_id)
    if equipe:
        delete_commit(equipe_deletada)
        return {}
    else:
        return {"ERROR":"insira dados de uma equipe da sua empresa"}    

def obter_equipes(empresa:object):
    lista = Equipes.query.filter_by(empresa_id=empresa.id).all()
    lista = [item.serialized for item in lista]
    
    return jsonify(lista)