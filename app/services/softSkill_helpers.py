from app.exc import (    
    MissingKeyError, 
    InvalidTokenError       
)
from app.models import SoftSkill,SoftSkillColaborador,Colaboradores
from .helpers import verify_missing_key, add_commit
from sqlalchemy.exc import IntegrityError

def criar_softSkill(data:dict,empresa:object):
    key_list=["nome"]
    if verify_missing_key(data,key_list):
        raise MissingKeyError(data,key_list)
    if empresa == None:
        raise InvalidTokenError()     
    try:
        new_skill = SoftSkill(**data)
        add_commit(new_skill) 
    except IntegrityError as e:
        return {"ERROR": "dados invalidos"} 

    return {
        "skill":data["nome"],        
        }            

def relacionar_softSkill(data:dict,empresa:object):
    key_list=["colaborador_id","soft_skill_id"]
    if verify_missing_key(data, key_list):
        raise MissingKeyError(data, key_list)
    if empresa == None:
        raise InvalidTokenError()
    try:
        new_relation=SoftSkillColaborador(**data)
        add_commit(new_relation)
    except IntegrityError as e:
        return {"ERROR": "dados invalidos"} 
    colaborador = Colaboradores.query.filter_by(id=new_relation.colaborador_id).first() 
    skill = SoftSkill.query.filter_by(id = new_relation.soft_skill_id).first()
    return {"colaborador": colaborador.nome,"hard_skill":skill.nome} 
