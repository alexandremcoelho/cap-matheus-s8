from app.exc import (    
    MissingKeyError,    
    StatusOptionsError,
)
from app.models import Status
from .helpers import verify_missing_key, add_all_commit, add_commit,invalid_status_option
from sqlalchemy.exc import IntegrityError
from flask import jsonify

def criar_status(data:dict):
    key_list = ["status"]
    status_list = ["não iniciado","em progresso","concluido"]
    if verify_missing_key(data, key_list):
        raise MissingKeyError(data, key_list)
    if invalid_status_option(data["status"],status_list):
        raise StatusOptionsError(data["status"],status_list)  
    try:
        new_status=Status(**data)
        add_commit(new_status)   
    except IntegrityError as e:
        return {"ERROR": "dados invalidos"}
    return {"status":new_status.status}         

def obter_status():
    status = Status.query.all()
    status=[item.serialized for item in status]
    return jsonify(status)