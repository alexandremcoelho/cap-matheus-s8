from operator import add
from http import HTTPStatus
from app.exc import MissingKeyError
from app.models import Equipes, EquipeTarefas, Tarefas
from flask import jsonify
from sqlalchemy.exc import IntegrityError

from .helpers import (add_all_commit, add_commit, verify_missing_key,
                      verify_recieved_key)


def criar_tarefa_equipe(data:dict):
    key_list=["tarefas_id", "equipe_id"]
    if verify_missing_key(data,key_list):
        raise MissingKeyError(data, key_list)
    try:
        equipe_tarefa = EquipeTarefas(**data)

        
        add_commit(equipe_tarefa)        
        
    except IntegrityError:
        return {"ERROR": "dados invalidos"} 
    
    return { "msg": "ok"
        }

def filter_task_by_id(tarefa_id: int):
    equipe_tarefas = EquipeTarefas.query.filter_by(tarefas_id=tarefa_id).all()
    
    tarefas = [Tarefas.query.filter_by(id=item.tarefas_id).first() for item in equipe_tarefas]
    filter_tarefas = []
    tarefas = [filter_tarefas.append(item) for item in tarefas if item not in filter_tarefas]
    filter_tarefas = [item.serialized for item in filter_tarefas]

    equipes = [Equipes.query.filter_by(id=item.equipe_id).first() for item in equipe_tarefas]
    filter_equipes = []
    tarefas = [filter_equipes.append(item) for item in equipes if item not in filter_equipes]
    filter_equipes = [item.serialized for item in filter_equipes]

    if len(filter_equipes) > 1:
        return {"informações_da_tarefa": filter_tarefas[0], "equipes_responsaveis": [item for item in filter_equipes] }
    
    return {"informações_da_tarefa": filter_tarefas[0], "equipe_responsavel": filter_equipes[0] }