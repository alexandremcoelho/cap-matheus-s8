from app.exc import (    
    MissingKeyError,    
    StatusOptionsError,
)
from app.models import Prioridade
from .helpers import verify_missing_key, add_all_commit, add_commit,invalid_status_option
from sqlalchemy.exc import IntegrityError
from flask import jsonify

def criar_prioridade(data:dict):
    key_list = ["prioridade"]
    priority_list = ["alta","media","baixa"]
    if verify_missing_key(data, key_list):
        raise MissingKeyError(data, key_list)
    if invalid_status_option(data["prioridade"],priority_list):
        raise StatusOptionsError(data["prioridade"],priority_list)  
    try:
        new_priority=Prioridade(**data)
        add_commit(new_priority)   
    except IntegrityError as e:
        return {"ERROR": "dados invalidos"}
    return {"prioridade":new_priority.prioridade}         

def obter_prioridades():
    prioridades = Prioridade.query.all()
    prioridade=[item.serialized for item in prioridades]
    return jsonify(prioridade)