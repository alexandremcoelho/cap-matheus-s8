from flask import Flask, current_app
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def init_app(app: Flask) -> None:
    db.init_app(app)
    app.db = db

    from app.models.colaboradores_model import Colaboradores
    from app.models.empresa_model import Empresa
    from app.models.equipe_tarefas_model import EquipeTarefas
    from app.models.equipes_model import Equipes
    from app.models.hard_skill_colaborador import HardSkillColaborador
    from app.models.hard_skill_model import HardSkill
    from app.models.prioridade_model import Prioridade
    from app.models.soft_skill_colaborador import SoftSkillColaborador
    from app.models.soft_skill_model import SoftSkill
    from app.models.tarefas_model import Tarefas
    
