import re
from . import db
from sqlalchemy.orm import relationship, backref

class SoftSkill(db.Model):
    __tablename__ = "soft_skill"
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(100), nullable=False)


    @property
    def serialized(self):
        return {
            "id": self.id,
            "nome": self.nome,            
        }