from . import db


class Prioridade(db.Model):
    __tablename__ = "prioridade"
    id = db.Column(db.Integer, primary_key=True)
    prioridade = db.Column(db.String(), nullable=False)
    tarefas = db.relationship("Tarefas", backref="prioridade")
    
    @property
    def serialized(self):
        return {
            "id": self.id,
            "prioridade":self.prioridade,
        }