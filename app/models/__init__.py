from app.configs.database import db

from .colaboradores_model import Colaboradores
from .empresa_model import Empresa
from .equipe_tarefas_model import EquipeTarefas
from .equipes_model import Equipes
from .hard_skill_colaborador import HardSkillColaborador
from .hard_skill_model import HardSkill
from .prioridade_model import Prioridade
from .soft_skill_colaborador import SoftSkillColaborador
from .soft_skill_model import SoftSkill
from .status_model import Status
from .tarefas_model import Tarefas
