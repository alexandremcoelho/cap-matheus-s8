from . import db


class Tarefas(db.Model):
    __tablename__ = "tarefa"
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(100), nullable=False)
    data_inicio = db.Column(db.Date(), nullable=False)
    data_fim = db.Column(db.Date(), nullable=False)
    descrição = db.Column(db.Text(), nullable=False)
    status_id = db.Column(db.Integer, db.ForeignKey("status.id"), nullable=True)
    equipe_id = db.Column(db.Integer(), db.ForeignKey("equipes.id"), nullable=True)
    prioridade_id = db.Column(
        db.Integer, db.ForeignKey("prioridade.id"), nullable=True
    )
    

    @property
    def serialized(self):
        return {
            "id": self.id,
            "nome": self.nome,
            "data_inicio": self.data_inicio,
            "data_fim": self.data_fim,
            "descrição": self.descrição,
        }
