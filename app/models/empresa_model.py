from werkzeug.security import check_password_hash, generate_password_hash

from . import db


class Empresa(db.Model):

    __tablename__ = 'empresa'

    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.Text, nullable=False)
    razao_social = db.Column(db.Text)
    nome_fantasia = db.Column(db.Text)
    cnpj = db.Column(db.Text, nullable=False, unique=True)
    telefone = db.Column(db.Text)
    email = db.Column(db.Text, nullable=False, unique=True)
    password_hash=db.Column(db.String,nullable=True)
    equipes = db.relationship("Equipes", backref="empresa")
    colaboradores = db.relationship("Colaboradores", backref="empresa")

    @property
    def serialized(self):
        return {
            "id": self.id,
            "nome": self.nome,
            "razao_social": self.razao_social,
            "nome_fantasia": self.nome_fantasia,
            "cnpj": self.cnpj,
            "telefone": self.telefone,
            "email": self.email
        }

    @property
    def password(self):
        raise AttributeError("Password cannot be accessed!")

    @password.setter
    def password(self, password_to_hash):
        self.password_hash = generate_password_hash(password_to_hash)

    def verify_password(self, password_to_compare):
        return check_password_hash(self.password_hash, password_to_compare)
