from . import db


class Status(db.Model):
    __tablename__ = "status"
    id = db.Column(db.Integer, primary_key=True)
    status = db.Column(db.String(200), nullable=False)
    tarefas = db.relationship("Tarefas", backref="status")

    
    @property
    def serialized(self):
        return {
            "id": self.id,
            "status": self.status,            
            "status":self.status,
        }