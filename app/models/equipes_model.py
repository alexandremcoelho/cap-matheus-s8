from . import db


class Equipes(db.Model):
    __tablename__ = "equipes"
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(100), nullable=False, unique=True)
    gerente_de_equipes = db.Column(db.String(100), nullable=False)
    descrição = db.Column(db.Text())
    empresa_id = db.Column(db.Integer(), db.ForeignKey("empresa.id"), nullable=False)
    especialização = db.Column(db.String(100), nullable=False)
    colaboradores = db.relationship("Colaboradores", backref="equipes")
    tarefas = db.relationship("Tarefas", backref="equipes")

    @property
    def serialized(self):
        return {
            "id": self.id,
            "nome": self.nome,
            "gerente_de_equipes": self.gerente_de_equipes,
            "descrição": self.descrição,
            "empresa_id": self.empresa_id,
            "especialização": self.especialização
        }