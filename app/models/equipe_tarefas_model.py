from . import db


class EquipeTarefas(db.Model):
    __tablename__ = "equipe_tarefas"
    id = db.Column(db.Integer(), primary_key=True)
    tarefas_id = db.Column(
        db.Integer(), db.ForeignKey("tarefa.id"), nullable=False
    )
    equipe_id = db.Column(
        db.Integer(), db.ForeignKey("equipes.id"), nullable=False
    )

