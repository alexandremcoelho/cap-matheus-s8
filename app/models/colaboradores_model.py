from . import db
from werkzeug.security import generate_password_hash, check_password_hash


class Colaboradores(db.Model):
    __tablename__ = 'colaboradores'
    id = db.Column(db.Integer(), primary_key=True)
    nome = db.Column(db.String(100), nullable=False)
    email = db.Column(db.String(200), nullable=False, unique=True)
    alocado = db.Column(db.Boolean(), nullable=False)
    empresa_id = db.Column(db.Integer(), db.ForeignKey("empresa.id"), nullable=False)
    equipe_id = db.Column(db.Integer(), db.ForeignKey("equipes.id"), nullable=True)
    colaborador_hard_skill = db.relationship(
        "HardSkill", secondary="hard_skill_colaborador", backref="colaboradores"
    )
    colaborador_soft_skill = db.relationship(
        "SoftSkill", secondary="soft_skill_colaborador", backref="colaboradores"
    )
    password_hash=db.Column(db.String,nullable=True)

    @property
    def serialized(self):
        return {
            "id": self.id,
            "nome": self.nome,           
            "email": self.email,
            "alocado":self.alocado,    
            "empresa":self.empresa_id,                 
        }

    @property
    def password(self):
        raise AttributeError("Password cannot be accessed!")

    @password.setter
    def password(self, password_to_hash):
        self.password_hash = generate_password_hash(password_to_hash)

    def verify_password(self, password_to_compare):
        return check_password_hash(self.password_hash, password_to_compare)
