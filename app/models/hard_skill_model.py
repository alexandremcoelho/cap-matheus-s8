from . import db


class HardSkill(db.Model):
    __tablename__ = "hard_skill"
    id = db.Column(db.Integer, primary_key=True)
    skill = db.Column(db.String(100), nullable=False)
    experiencia = db.Column(db.String(100), nullable=False)
    @property
    def serialized(self):
        return {
            "id": self.id,
            "skill": self.skill,            
            "experiencia": self.experiencia
        }

        