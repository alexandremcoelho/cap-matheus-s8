from . import db


class HardSkillColaborador(db.Model):
    __tablename__ = "hard_skill_colaborador"
    id = db.Column(db.Integer(), primary_key=True)
    colaborador_id = db.Column(
        db.Integer(), db.ForeignKey("colaboradores.id"), nullable=False
    )
    hard_skill_id = db.Column(
        db.Integer(), db.ForeignKey("hard_skill.id"), nullable=False
    )
