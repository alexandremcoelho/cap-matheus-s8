from . import db


class SoftSkillColaborador(db.Model):
    __tablename__ = "soft_skill_colaborador"
    id = db.Column(db.Integer, primary_key=True)
    colaborador_id = db.Column(
        db.Integer(), db.ForeignKey("colaboradores.id"), nullable=False
    )
    soft_skill_id = db.Column(db.ForeignKey("soft_skill.id"), nullable=False)
