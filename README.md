# Introdução
Essa documentação tem com objetivo ajudar os desenvolvedores de Front-End a encontrar e entender as rotas e seus retornos.


## Empresa

<!--Tabelas Empresas-->

|chave          | Tipo   | Descrição            |
|-----          |------  |------------------    |
|id             | int    | Id da Empresa        |
|nome           |string  | Nome da Empresa      |
|razao_social   |string  | Nome oficial empresa |
|nome_fantasia  |string  | Nome Fantasia da empresa        |
|cnpj           |string  | CNPJ empresa        |
|telefone       |strig   | Telefone empresa   |
|email          |strig   | Email Empresa      |



## Post - Registrando uma nova empresa
Para criar uma empresa é necessário estar logado, assim que se loga um token é gerado e esse token expira e "***". Depois diss o login se faz necessário novamente.


```python
    POST https://capstnegestaodevs8.herokuapp.com/empresa

    {
    "nome": "Dev Company",
    "razao_social": "1321654646131221",
    "nome_fantasia": "Dev Company JR",
    "cnpj": "123.000.000-12/0000",
    "telefone": "31-2132456",
    "email": "devcompany@gmail.com",
		"password": "dev123"
 }
 
```
## retorno

```python

{
  "id": 30,
  "nome": "Dev Company",
  "razao_social": "1321654646131221",
  "nome_fantasia": "Dev Company JR",
  "cnpj": "123.000.000-12/0000",
  "telefone": "31-2132456",
  "email": "devcompany@gmail.com"
}

 
```


## POST - login Empresa



```python
    POST https://capstnegestaodevs8.herokuapp.com/empresa/login
 {
 "email": "devcompany@gmail.com",
"password": "dev123"
 
}
```
## Retorno
```python
{
  "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmcmVzaCI6ZmFsc2UsImlhdCI6MTYyNjg5NzU2MSwianRpIjoiODEwMzcwYzMtZjJiMC00M2ZlLTgxN2YtODM2OTYzZWQwMjMzIiwidHlwZSI6ImFjY2VzcyIsInN1YiI6ImRldmNvbXBhbnlAZ21haWwuY29tIiwibmJmIjoxNjI2ODk3NTYxLCJleHAiOjE2MjY4OTg0NjF9.kvxVcNwS5dwZmaljvKYpac42ijvnWPgbvogcQapgXBk"
}

```
### Erro -  Caso envie um email ou senha incorreto

```python
{
  "message": "incorrect email or password"
}
 
```


## GET- Listando todas as empresas

Acesando o endepoint ` /empresas` lista-se todas as empresas cadastradas.

```python
    GET https://capstnegestaodevs8.herokuapp.com/empresas
 
```

```python
[
  {
    "id": 1,
    "nome": "esparta3",
    "razao_social": "nao sei o que era pra ser isso",
    "nome_fantasia": "trezentos",
    "cnpj": "acho que era pra ser um numero",
    "telefone": "na duvida virou string tb",
    "email": "teste4@mail.com"
  },
  {
    "id": 2,
    "nome": "Gestao ninja",
    "razao_social": "gestao agil",
    "nome_fantasia": "Gestao ninja",
    "cnpj": "76.105.837/0001-67",
    "telefone": "99931-1708",
    "email": "empresa2@gmail.com"
  },
  {
    "id": 3,
    "nome": "espartass",
    "razao_social": "naos sei o que era pra ser isso",
    "nome_fantasia": "trezesntos",
    "cnpj": "acho ques era pra ser um numero",
    "telefone": "nas duvida virou string tb",
    "email": "testes5@mail.com"
  },
  {
    "id": 6,
    "nome": "espartasss",
    "razao_social": "naos sei o que era pra ser isso",
    "nome_fantasia": "trezesntos",
    "cnpj": "acho ques era pra ser um numeros",
    "telefone": "nas duvida virou string tb",
    "email": "testess5@mail.com"
  },
  {
    "id": 10,
    "nome": "espartasss",
    "razao_social": "naos sei o que era pra ser isso",
    "nome_fantasia": "trezesntos",
    "cnpj": "achso ques era pra ser um numeros",
    "telefone": "nas duvida virou string tb",
    "email": "testesss5@mail.com"
  },
  {
    "id": 26,
    "nome": "minha empresa",
    "razao_social": "empresa minha",
    "nome_fantasia": "Gestao empresa",
    "cnpj": "76.105.837/0001-68",
    "telefone": "99931-1708",
    "email": "empresa1@gmail.com"
  },
  {
    "id": 29,
    "nome": "esparstassss",
    "razao_social": "naos sei o ques era pra ser isso",
    "nome_fantasia": "treszesnstos",
    "cnpj": "achso qsuess era pra ser um numeros",
    "telefone": "nas duvida virou string tb",
    "email": "testesssss5@mail.com"
  },
  {
    "id": 30,
    "nome": "Dev Company",
    "razao_social": "1321654646131221",
    "nome_fantasia": "Dev Company JR",
    "cnpj": "123.000.000-12/0000",
    "telefone": "31-2132456",
    "email": "devcompany@gmail.com"
  }
]
 
```

## DELETE - Deletando empresas

Para deleta uma empresa é necessário que a empresa esteja com o token valido.

```python
    
    DELETE https://capstnegestaodevs8.herokuapp.com/delete/1

```

```python
    
    {}

```

## Erros Gerais


### Erro-01 - Assinatura vencidada 

```python
    
   {
  "msg": "Signature verification failed"
}

```
### Erro-02 - Id de uma empresa que não existe.

```python
    
{
  "ERROR": "id invalido"
}
```
### Erro-03 Tentar inserir dados inválidos

Ao tentar cadastrar campos que não existem no banco obtem-se a seguinte resposta: 

```python
    POST https://capstnegestaodevs8.herokuapp.com/empresa

{
    "nome": "Dev Company",
    "razao_social": "1321654646131221",
    "nome_fantasia": "Dev Company JR",
    "cnpj": "123.000.000-12/0000",
    "telefone": "31-2132456",
    "email": "devcompany@gmail.com",
		"password": "dev123"
 }
```
```python
   
{
  "error": {
    "missing_keys": [
      "nome",
      "razao_social",
      "nome_fantasia",
      "cnpj",
      "telefone",
      "email",
      "password"
    ],
    "recieved_keys": [
      "nome",
      "razao_social",
      "nome_fantasia",
      "cnpj",
      "telefone",
      "email",
      "password_hast"
    ]
  }
}
```


## Colaborador
Para ter acesso aos enpoints do colaborador o colaborador deve estar logado execeto no caso em que o colaborador vai se cadastrar.
<!--Tabelas Empresas-->

|chave          | Tipo   | Descrição            |
|-----          |------  |------------------    |
|id             | int    | Id do colaborador    |
|nome           |string  | Nome do colaborador  |
|empresa_d      |string  | Empresa na qual colaborador trabalha   |
|email          |strig   | Email colaborador    |
|alocado        |boolean | se o colaboraor já esta alocado em algum projeto |
|password  |string  | Nome Fantasia        |

## POST - Registrando um novo colaborador
Para criar os campos nem todos os dados são necessários, porém nome, email e senha sim. Importante salietar aqui que temos que informar o id de qual empresa o colaborador esta sendo cadastrado.

```python
    POST https://capstnegestaodevs8.herokuapp.com/colaboradores/create_acount/1
    

 
```
## Retorno
```python
{
  "nome": "Dev01",
  "email": "dev01@gmail.com",
  "alocado": false,
  "empresa": "nao sei o que era pra ser isso"
}
```

## POST - Login colaborador

```python
    POST https://capstnegestaodevs8.herokuapp.com/colaborador/login

{
	"email":"dev01@gmail.com",
	"password":"dev123"
}
 
```
## Retorno
```python
{
  "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJmcmVzaCI6ZmFsc2UsImlhdCI6MTYyNjg5ODY5NSwianRpIjoiMTU4MjFiZjEtNTQ4NS00MzYwLTgxZjktNGRjMjQ5N2E4YWJlIiwidHlwZSI6ImFjY2VzcyIsInN1YiI6ImRldjAxQGdtYWlsLmNvbSIsIm5iZiI6MTYyNjg5ODY5NSwiZXhwIjoxNjI2ODk5NTk1fQ.A9pNf6T41jZv_j4s2uMV02fqVJpv8XPHNN_8qBR54cA"
}
 
```


## GET - Lista todos os colaboradores

Essa rota exige que o colaborador esteja logado e com o token válido. 
```python
    GET https://capstnegestaodevs8.herokuapp.com/colaboradores
 
```
```python
[
  {
    "id": 7,
    "nome": "Lucas",
    "email": "lucasjac13@gmail.com",
    "alocado": false,
    "empresa": "nao sei o que era pra ser isso"
  },
  {
    "id": 8,
    "nome": "Lucaas",
    "email": "lucasjaca122@gmail.com",
    "alocado": false,
    "empresa": "nao sei o que era pra ser isso"
  },
  {
    "id": 9,
    "nome": "Lucaas",
    "email": "lucasjacza122@gmail.com",
    "alocado": false,
    "empresa": "nao sei o que era pra ser isso"
  },
  {
    "id": 11,
    "nome": "Lucas",
    "email": "lucasjac143@gmail.com",
    "alocado": false,
    "empresa": "nao sei o que era pra ser isso"
  },
  {
    "id": 12,
    "nome": "Dev01",
    "email": "dev01@gmail.com",
    "alocado": false,
    "empresa": "nao sei o que era pra ser isso"
  }
]
 
```


## PATCH - Relaciondo um colaborador a um equipe
Necessário token valido

```python
{
  "colaborador_id":12,
  "equipe_id":1
}
 
```

### Erro - Token inválido
```python
{
  "msg": "Signature verification failed"
}
 
```

## PATCH - Relaciondo um colaborador a um Soft Skill
Necessário token valido

```python
{
  "colaborador_id":1,
  "soft_skill_id":1
}
 
```
## retorno
```python
{
  "colaborador": "Dev",
  "hard_skill": "empatia"
}
```
## PATCH - Relaciondo um colaborador a um Hard Skill
Necessário token valido

```python
{
  "colaborador_id":12,
  "hard_skill_id":1
}
 
```


```python
{
  "msg": "Signature verification failed"
}
 
```


## Equipes

|chave          | Tipo   | Descrição            |
|-----          |------  |------------------    |
|id             | int    | Id do colaborador    |
|nome           |string  | Nome do colaborador  |
|cliente        |string  | Empresa para qual equipe esta trabalhando |
|gerente_equipe |strig   | Responsável pela gestão da equipe    |
|Calaboradores  |string  | Colaboradores da equipe |
|tecnologias    |string  | Tecnologias adotadas pela equipe      |
|Especialização_id |string | Especialização da equipe ('back', 'front') |
|empresa_9d   |integer  | Id da empresa responsável pela equipe e projeto  |

## POST - Criando equipe

```python
    POST https://capstnegestaodevs8.herokuapp.com/equipes

{
 "nome":"equipe devs01",
  "gerente_de_equipes":"Lucas jacino",
  "descrição":"a equipe mais braba da empresa",
  "empresa_id":1,
  "especialização":"back-end"
}


```
retorno

```python
{
  "name": "equipe devs01",
  "descricao": "a equipe mais braba da empresa"
}
 
```

### Error - Campos inválidos

```python
{
  "error": {
    "missing_keys": [
      "nome",
      "gerente_de_equipes",
      "descrição",
      "empresa_id",
      "especialização"
    ],
    "recieved_keys": [
      "nome1",
      "gerente_de_equipes",
      "descrição",
      "empresa_id",
      "especialização"
    ]
  }
}
 
```


## GET - Listando equipes
```python
    GET https://capstnegestaodevs8.herokuapp.com/equipes

{
 "nome":"equipe devs01",
  "gerente_de_equipes":"Lucas jacino",
  "descrição":"a equipe mais braba da empresa",
  "empresa_id":1,
  "especialização":"back-end"
}
```
## DELETE - Deletando equipe
```python
    GET https://capstnegestaodevs8.herokuapp.com/equipes/deletar_equipe/3

[
  {
    "id": 3,
    "nome": "Devs 01",
    "gerente_de_equipes": "Lucas jacino",
    "descrição": "a equipe mais braba da empresa",
    "empresa_id": 2,
    "especialização": "back-end"
  },
  {
    "id": 4,
    "nome": "Devs 02",
    "gerente_de_equipes": "Alexandre",
    "descrição": "a equipe mais braba da empresa",
    "empresa_id": 2,
    "especialização": "back-end"
  },
  {
    "id": 5,
    "nome": "Devs 03",
    "gerente_de_equipes": "Gabriel",
    "descrição": "a equipe mais braba da empresa",
    "empresa_id": 2,
    "especialização": "back-end"
  }
]
```

## POST- Equipe Tarefa
Por enquanto com erros

## HardSkill 
|chave          | Tipo   | Descrição            |
|-----          |------  |------------------    |
|id             | int    | Id do Status         |
|skill          |string  | Nome da Skill        | 
|experiencia    |string  | Nível de experiência do colaboraodr na skill('baixa', 'media', 'alta')


## POST- Criando Hard Skill
Para criar a Hard Skill o colaboraor deve estar logado e com um token valído.
```python
    POST https://capstnegestaodevs8.herokuapp.com/hard_skill
 
```

```python
{
	"skill": "selfa.skill",
    "experiencia": "alta"
}
 
```

## GET - Listando as Hard Skills
Para listas as Hard Skills o colaboraor deve estar logado e com um token valído.
```python
    GET https://capstnegestaodevs8.herokuapp.com/hard_skill
 
```

```python
    [
  {
    "id": 1,
    "skill": "React",
    "experiencia": "alta"
  },
  {
    "id": 2,
    "skill": "HTML",
    "experiencia": "alta"
  },
  {
    "id": 3,
    "skill": "JavaScript",
    "experiencia": "Média"
  }
]
 
```

## SoftSkill 

|chave          | Tipo   | Descrição            |
|-----          |------  |------------------    |
|id             | int    | Id da skill        |
|skill          |string  | Nome da Skill          |



## Tarefas 


|chave          | Tipo   | Descrição            |
|-----          |------  |------------------    |
|id             | int    | Id do colaborador    |
|nome           |string  | Nome do colaborador  |
|data_inicio    |string  | borador trabalha   |
|data_fim       |strig   | Email colaborador |
|descricao      |boolean | cado em algum projeto |
|status_id      |integer | Status tarefa('pendente','em andamente', 'concluído')        |
|prioridade_id  |integer | Prioridade tarefa ('Baixa','Média','Alta','Critíca')
|equipe_id      | integer| Equipe a qual a tarefa se relaciona 


## POST - Criando tarefas

Cadastro de tarefa sem relação com as tabelas status, priopridade e equipe

```python
    POST https://capstnegestaodevs8.herokuapp.com/tarefa
 {
  "nome":"construir front-end",
  "data_inicio":"2012-04-23T18:25:43.511Z",
  "data_fim":"2012-06-23T18:25:43.511Z",
  "descrição":"construir um front-end bonito"
}
 
``` 
## Retorno

```python
{
  "nome": "construir front-end",
  "data_inicio": "Mon, 23 Apr 2012 00:00:00 GMT",
  "data_fim:": "Sat, 23 Jun 2012 00:00:00 GMT",
  "descrição": "construir um front-end bonito"
}
 
``` 
## Error - Campos inválidos
Caso envie campos que não são aceitos pelo banco obtem-se o seguinte mensagem:
```python
{
  "error": {
    "missing_keys": [
      "nome",
      "data_inicio",
      "data_fim",
      "descrição"
    ],
    "recieved_keys": [
      "nome1",
      "data_inicio",
      "data_fim",
      "descrição"
    ]
  }
}
 
``` 
## PATCH - Atulizar tarefa


```python
    POST https://capstnegestaodevs8.herokuapp.com/tarefa/atualizar_tarefa
{	
	"id":1,
	"prioridade_id":1,
	"status_id":1,
	"equipe_id":1
}
 
``` 

## DELETE - Deletar tarefa

```python
    POST https://capstnegestaodevs8.herokuapp.com/tarefa/delete/7
```
## Retorno
```python
    {}
```

## GET - Lista todas as tarefas


```python
    POST https://capstnegestaodevs8.herokuapp.com/tarefas

```
## retorno 
```python
    [
  {
    "id": 2,
    "nome": "tarefa aleatoria",
    "data_inicio": "Mon, 10 Oct 2011 00:00:00 GMT",
    "data_fim": "Mon, 10 Oct 2011 00:00:00 GMT",
    "descrição": "dificil"
  },
  {
    "id": 3,
    "nome": "tarefa aleatoria",
    "data_inicio": "Mon, 10 Oct 2011 00:00:00 GMT",
    "data_fim": "Mon, 10 Oct 2011 00:00:00 GMT",
    "descrição": "dificil"
  },
  {
    "id": 4,
    "nome": "construir front-end",
    "data_inicio": "Mon, 23 Apr 2012 00:00:00 GMT",
    "data_fim": "Sat, 23 Jun 2012 00:00:00 GMT",
    "descrição": "construir um front-end agradavel para a pagina"
  },
  {
    "id": 5,
    "nome": "tarefa aleatoria 3",
    "data_inicio": "Mon, 10 Oct 2011 00:00:00 GMT",
    "data_fim": "Mon, 10 Oct 2011 00:00:00 GMT",
    "descrição": "dificil"
  },
  {
    "id": 6,
    "nome": "construir front-end",
    "data_inicio": "Mon, 23 Apr 2012 00:00:00 GMT",
    "data_fim": "Sat, 23 Jun 2012 00:00:00 GMT",
    "descrição": "construir um front-end bonito"
  },
  {
    "id": 7,
    "nome": "construir front-end",
    "data_inicio": "Mon, 23 Apr 2012 00:00:00 GMT",
    "data_fim": "Sat, 23 Jun 2012 00:00:00 GMT",
    "descrição": "construir um front-end bonito"
  }
]

``` 

## GET - Filtro - Tarefa status e prioridade


```python
    POST https://capstnegestaodevs8.herokuapp.com/tarefa?status?status=nao iniciad&prioridade=baixa

``` 
## retorno
```python
    [
  {
    "id": 2,
    "nome": "tarefa aleatoria",
    "data_inicio": "Mon, 10 Oct 2011 00:00:00 GMT",
    "data_fim": "Mon, 10 Oct 2011 00:00:00 GMT",
    "descrição": "dificil"
  },
  {
    "id": 3,
    "nome": "tarefa aleatoria",
    "data_inicio": "Mon, 10 Oct 2011 00:00:00 GMT",
    "data_fim": "Mon, 10 Oct 2011 00:00:00 GMT",
    "descrição": "dificil"
  },
  {
    "id": 4,
    "nome": "construir front-end",
    "data_inicio": "Mon, 23 Apr 2012 00:00:00 GMT",
    "data_fim": "Sat, 23 Jun 2012 00:00:00 GMT",
    "descrição": "construir um front-end agradavel para a pagina"
  },
  {
    "id": 5,
    "nome": "tarefa aleatoria 3",
    "data_inicio": "Mon, 10 Oct 2011 00:00:00 GMT",
    "data_fim": "Mon, 10 Oct 2011 00:00:00 GMT",
    "descrição": "dificil"
  },
  {
    "id": 6,
    "nome": "construir front-end",
    "data_inicio": "Mon, 23 Apr 2012 00:00:00 GMT",
    "data_fim": "Sat, 23 Jun 2012 00:00:00 GMT",
    "descrição": "construir um front-end bonito"
  },
  {
    "id": 7,
    "nome": "construir front-end",
    "data_inicio": "Mon, 23 Apr 2012 00:00:00 GMT",
    "data_fim": "Sat, 23 Jun 2012 00:00:00 GMT",
    "descrição": "construir um front-end bonito"
  }
]

``` 

## POST - Tarefa Prioridade


## Status
|chave          | Tipo   | Descrição            |
|-----          |------  |------------------    |
|id             | int    | Id do Status         |
|nome           |string  | Nome Status          |


## POST - Status

```python
    POST https://capstnegestaodevs8.herokuapp.com/status

{
    "status":"não iniciado"
}
 
```
Os valores da tabela status já foram predefinidos, caso alguém tente cadastrar valores diferes obtem-se a seguinte mensagem:

```python
    POST https://capstnegestaodevs8.herokuapp.com/status

{
    "status":"não iniciado"
}
 
```

```python
    POST https://capstnegestaodevs8.herokuapp.com/status

{
  "error": {
    "status_options": [
      "não iniciado",
      "em progresso",
      "concluido"
    ],
    "recieved_option": "Pendente"
  }
}

```

## GET - Status


```python
    GET https://capstnegestaodevs8.herokuapp.com/status/

```

```python
    GET https://capstnegestaodevs8.herokuapp.com/status/

    [
  {
    "id": 1,
    "status": "pendente"
  },
  {
    "id": 2,
    "status": "em andamento"
  },
  {
    "id": 3,
    "status": "concluido"
  },
  {
    "id": 4,
    "status": "feedback"
  },
  {
    "id": 5,
    "status": "não iniciado"
  }
]

```

## Prioridade

|chave          | Tipo   | Descrição            |
|-----          |------  |------------------    |
|id             | int    | Id da Prioridade     |
|nome           |string  | Nome das Prioriade   |

## POST - Criando uma prioridadse

```python
    POST https://capstnegestaodevs8.herokuapp.com/prioridade

{
	"prioridade":"baixa"
}

```
## Retorno
```python
 {
	"prioridade":"baixa"
}

```


### GET - Lista todas as prioridades

```python
    GET https://capstnegestaodevs8.herokuapp.com/prioridade

```
## Retorno
```python
[
  {
    "id": 1,
    "prioridade": "baixa"
  },
  {
    "id": 2,
    "prioridade": "média"
  },
  {
    "id": 3,
    "prioridade": "alta"
  },
  {
    "id": 4,
    "prioridade": "critica"
  },
  {
    "id": 5,
    "prioridade": "baixa"
  }
]

```

